<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Jhay Reyes',
            'username' => 'njhay15',
            'email' => 'nj15reyes@gmail.com',
            'password' => bcrypt('killall123'),
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Aidyl Jean',
            'username' => 'aidyljean15',
            'email' => 'aidyljeanv@gmail.com',
            'password' => bcrypt('laravel'),
        ]);
    }
}
